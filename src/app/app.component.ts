import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Router } from '@angular/router';

import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	constructor(private auth: AuthService, private router: Router) {}
	title = 'Cardihab';

	logout() : void {
        this.auth.logout();
        return this.router.navigateByUrl('/home');
    }

    isLoggedOut() : void {
        return this.auth.isLoggedOut();
    }

    isLoggedIn() : void {
        return this.auth.isLoggedIn();
    }
}
