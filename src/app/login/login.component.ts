import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

import { MedicationComponent } from '../medication/medication.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private auth: AuthService, private router: Router) {}
  user: any = {}
  const token = ''
  onLogin(this.user): void {
    if (this.user) {
      this.auth.login(this.user.username, this.user.password)
        .subscribe(
          (data) => {
            console.log("User is logged in");
            this.router.navigateByUrl('/medication');
            this.MedicationComponent.searchMedicine(data._body, this.user.medications);
            console.log(data);
          }
        );
    }
  }
}
