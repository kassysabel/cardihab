import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { MedicationService } from '../services/medication.service';

@Component({
  selector: 'app-medication',
  templateUrl: './medication.component.html',
  styleUrls: ['./medication.component.css']
})
export class MedicationComponent implements OnInit {
	constructor(private medService: MedicationService, private router: Router) {}

	const medicine: string = 'Biogesic';
	const token: string = 'token';
	user: any = {};
	searchMedicine(token, medicine) : void {
		this.medService.ensureAuthenticated(this.token, this.medicine);
	}
}
