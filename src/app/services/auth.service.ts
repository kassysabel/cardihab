import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { toPromise } from 'rxjs';
import { do } from 'rxjs/add/operator/do';
import * as moment from 'moment';

@Injectable()
export class AuthService {
    private BASE_URL: string = 'https://api-dev.cardihab.app/v1/login';
    private headers: Headers = new Headers({'Content-Type': 'application/json'});
    constructor(private http: Http) {}


    login(username, password): Promise<any> {
    let url: string = `${this.BASE_URL}`;
    let user: any = {username: username, password: password};
    this.setSession(this.http.post(url, user, {headers: this.headers});
    console.log(this.headers);
    return this.http.post(url, user, {headers: this.headers});    
  }

    private setSession(authResult) {
      const expiresAt = moment().add(86400,'second');
      localStorage.setItem('id_token', authResult.idToken);
      localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
    }

    ensureAuthenticated(token): Promise<any> {
      let url: string = `${this.BASE_URL}/home`;
      let headers: Headers = new Headers({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      });
      return this.http.get(url, {headers: headers}).toPromise();
    }

    public logout() {
        localStorage.removeItem("id_token");
        localStorage.removeItem("expires_at");
    }

    public isLoggedIn() {
        return moment().isBefore(this.getExpiration());
    }

    public isLoggedOut() {
        return !this.isLoggedIn();
    }

    getExpiration() {
        const expiration = localStorage.getItem("expires_at");
        const expiresAt = JSON.parse(expiration);
        return moment(expiresAt);
    }
}