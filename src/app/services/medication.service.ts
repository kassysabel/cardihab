import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { toPromise } from 'rxjs';
import { do } from 'rxjs/add/operator/do';
import * as moment from 'moment';


@Injectable()
export class MedicationService {
    private BASE_URL: string = 'https://api-dev.cardihab.app/v1/medications/search?count=100&term=';
    private headers: Headers = new Headers({'Content-Type': 'application/json'});
    constructor(private http: Http) {}


    ensureAuthenticated(token, medicine): Promise<any> {
      let url: string = `${this.BASE_URL}${medicine}`;
      let headers: Headers = new Headers({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      });
      return this.http.get(url, {headers: headers});
    }
}